﻿using UnityEngine;

 [CreateAssetMenu(fileName = "PopGameData", menuName = "PopGame/TestingData/PopData", order = 1)]
public class PopGameData : ScriptableObject
{
    public int ProgressBarVal;
    public int ProgressBarFullVal;
    public bool BombOn;

}
