﻿using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
public class GameUIOverlay : BaseUIOverlay
{
    Text _progressText;
    Button _bombButton;
    ProgressLevel _barFillLevel;
    bool _bombEnable;
    public Sprite StarLightOnImg;
    public GameObject StarParticleEffect;
    public GameObject GameFinishEffect;
    protected override void Start()
    {
        base.Start();
        GetCertainButton("Bomb");
        GetCertainText("CentreText");
        GetProgressFillImg();
        BombEnable();

    }
    public void GameFinish()
    {
        BombDisable();
        PlayGameFinishEffect();
    }
    ///////////progress bar
    void GetProgressFillImg()
    {
        _barFillLevel = GetComponentInChildren<ProgressLevel>();
        if ((_barFillLevel == null) || (_barFillLevel.name != "bar_fill"))
        {
            Debug.Log("No ProgressLevel in Overlay");
            return;
        }

    }
    public void UpdateBarFillLevel(float levelRatio)
    {

        _barFillLevel.UpdateShiftAmount(levelRatio);

    }
    protected override void GetCertainText(string textName)
    {
        base.GetCertainText(textName);
        _progressText = _findText;

    }

    public void SetTextValue(int value)
    {
        _progressText.text = value.ToString();
    }
    ////////////Bomb reactions
    protected override void GetCertainButton(string buttonName)
    {
        base.GetCertainButton(buttonName);
        _bombButton = _findButton;
    }
    public bool CheckBombDown(Vector2 downPos)
    {
        if (_bombEnable)
        {
            if (CheckButtonDown(_bombButton, downPos))
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        else
        {
            return false;
        }
    }
    public void PlayBombScaleUpAnimation()
    {
        if (_bombButton != null)
        {
            _bombButton.transform.parent.DOScale(1.1f, .25f).OnComplete(OnBombScaleDown);
        }
    }
    void OnBombScaleDown()
    {
        _bombButton.transform.parent.DOScale(1, .25f).OnComplete(OnAnimComplete);
    }
    public void BombEnable()
    {
        _bombEnable = true;
    }

    public void BombDisable()
    {
        _bombEnable = false;
        _bombButton.transform.GetComponent<Image>().DOColor(Color.grey, 0.5f);
    }


    /////////////////Stars Reactions
    public void StarLightOn(int starIndex)
    {
        //with Particle effect & shader
        float currentStarScale = _stars[starIndex].transform.localScale.x;
        _lightOnStars[starIndex] = Instantiate(_stars[starIndex].gameObject, _stars[starIndex].transform.position, Quaternion.identity).GetComponent<Star>();
        _lightOnStars[starIndex].transform.parent = _stars[starIndex].transform.parent;
        PlayStarEffect(_lightOnStars[starIndex]);
        _lightOnStars[starIndex].GetComponent<Image>().sprite = StarLightOnImg;
        _lightOnStars[starIndex].transform.localScale = new Vector3(0.01f, 0.01f, 0.01f);
        Sequence starAnimSquence = DOTween.Sequence();
        starAnimSquence.Append(_lightOnStars[starIndex].transform.DOScale(currentStarScale + 0.5f, .5f).SetEase(Ease.OutBounce));
        starAnimSquence.Append(_lightOnStars[starIndex].transform.DOScale(currentStarScale, .15f).OnComplete(OnAnimComplete));

    }
    void PlayLightOnAnimation(Star curStar)
    {

    }
    //////////////////////////create Particle Effect
    public void PlayGameFinishEffect()
    {
        Instantiate(GameFinishEffect, Vector3.zero, Quaternion.identity);
    }
    void PlayStarEffect(Star parentStar)
    {
        GameObject effect = Instantiate(StarParticleEffect, Vector3.zero, Quaternion.identity);
        effect.transform.parent = parentStar.transform;
        effect.GetComponent<RectTransform>().localPosition = new Vector3(0, 0, -1);
    }

}
