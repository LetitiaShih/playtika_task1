﻿using UnityEngine;
using UserInput;
using UnityEngine.Events;
using DG.Tweening;
[System.Serializable]
public class PopUpOverlayEvent : UnityEvent<PopUpOverlay>
{
}
public class GameUIManager : BaseGameUIManager
{
    PopUpOverlay _popUpFinishOverlayPrefab;
    PopUpOverlay _popUpFinishOverlayIns;
    GameUIOverlay _popGameOverlay;
    bool isAnimating = false;
    protected override void Start()
    {
        base.Start();
        _popGameOverlay = GameObject.FindWithTag("View").GetComponent<GameUIOverlay>();
        _popGameOverlay.AnimCompleteEvent.AddListener(OnAnimComplete);
        _popUpFinishOverlayPrefab = Resources.Load<PopUpOverlay>("Prefabs/PopUpWindow");
    }
    protected override void GetData()
    {
        base.GetData();
    }
    void OnBombClicked()
    {
        _popGameOverlay.PlayBombScaleUpAnimation();
    }
    void Update()
    {
        if (!isAnimating)
        {
            if (_inputManager.TapDown)
            {
                if (_popUpFinishOverlayIns != null && _popUpFinishOverlayIns.CheckFinishDown(_inputManager.TapPos))
                {
                    Destroy(_popUpFinishOverlayIns.gameObject);
                }
                else if (_popGameOverlay.CheckBombDown(_inputManager.TapPos))
                {
                    isAnimating = true;
                    OnBombClicked();
                    IncreaseBarValue();
                }
                ////if cat animating
            }
            else if (_inputManager.TapUp)
            {
                //Debug.Log("Tap released");
            }
        }
    }
    void IncreaseBarValue()
    {
        _progressBarVal++;
        switch (_progressBarVal)
        { //case conditions can be _progressBarFullVal/stars number (take care of float number) (use if-else conditions)
            case 1:
                _popGameOverlay.StarLightOn(0);
                _popGameOverlay.UpdateBarFillLevel(GetRatioOfProgress());
                _popGameOverlay.SetTextValue(_progressBarVal);
                //call View the play particle effect
                break;
            case 2:
                _popGameOverlay.StarLightOn(1);
                _popGameOverlay.UpdateBarFillLevel(GetRatioOfProgress());
                _popGameOverlay.SetTextValue(_progressBarVal);
                break;
            case 3:
                _popGameOverlay.StarLightOn(2);
                _popGameOverlay.UpdateBarFillLevel(GetRatioOfProgress());
                _popGameOverlay.SetTextValue(_progressBarVal);
                OnGameFinish();
                break;
            default:
                Debug.Log("Exception case");
                break;

        }
    }
    public float GetRatioOfProgress()
    {
        return (float)_progressBarVal / (float)_progressBarFullVal;
    }
    void OnAnimComplete()
    {
        isAnimating = false;
        //Debug.Log("Anim Complete");
    }
    protected override void OnGameFinish()
    {
        base.OnGameFinish();
        // DOTween.KillAll();
        //TODO: mainGame WhiteOut
        _popGameOverlay.GameFinish();
        Invoke("FinishOverlaySetUp", 2);

    }

    /////////////PopUpWindowControl, Can have more pop-up overlays eg.Pause
    ///GameFinishWindow  
    void FinishOverlaySetUp()
    {
        _popUpFinishOverlayIns = Instantiate(_popUpFinishOverlayPrefab, Vector2.zero, Quaternion.identity);
        _popUpFinishOverlayIns.AnimCompleteEvent.AddListener(OnAnimComplete);
        ListenToPopUpFinishButtonEvents();
        isAnimating = true;
    }
    void ListenToPopUpFinishButtonEvents()
    {
        if (_popUpFinishOverlayIns != null)
        {
            _popUpFinishOverlayIns.FinishEvent.AddListener(PopUpComplete);
        }
    }
    void PopUpComplete(PopUpOverlay popUpOverlay)
    {
        Destroy(popUpOverlay.gameObject);
    }

}
