﻿using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;
using DG.Tweening;
public class PopUpOverlay : BaseUIOverlay
{
    public PopUpOverlayEvent FinishEvent;
    Button _finishButton;
    bool _finishEnable;
    public Animator CatAnimator;
    public RectTransform BG;
    bool checkAnimEnd = false;
    protected override void Start()
    {
        base.Start();
        //jumpOut anim
        GetCertainButton("FinishButton");
        FinishDisable();
        OverlayAppearAnimation();
    }
    void OverlayAppearAnimation()
    {
        BG.localScale = new Vector3(0.01f, 0.01f, 0.01f);
        Sequence starAnimSquence = DOTween.Sequence();
        starAnimSquence.Append(BG.DOScale(1.2f, 1f).SetEase(Ease.OutQuart));
        starAnimSquence.Append(BG.DOScale(1, .1f).OnComplete(PlayCatAnimation));
    }
    ////////////////Cat Animation
    void PlayCatAnimation()
    {
        CatAnimator.gameObject.SetActive(true);
        Invoke("PlayCat2ndAnimation", 5);
    }
    void PlayCat2ndAnimation()
    {
        CatAnimator.SetBool("CatAction", true);
        checkAnimEnd = true;
    }

    ///////////////Finish Button Setting
    protected override void GetCertainButton(string buttonName)
    {
        base.GetCertainButton(buttonName);
        _finishButton = _findButton;
    }
    public bool CheckFinishDown(Vector2 downPos)
    {
        if (_finishEnable)
        {
            if (CheckButtonDown(_finishButton, downPos))
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        else
        {
            return false;
        }
    }
    public void FinishEnable()
    {
        _finishEnable = true;
        _finishButton.gameObject.SetActive(true);
        FinishButtonAppearAnim();
    }
    public void FinishDisable()
    {
        _finishEnable = false;
        _finishButton.transform.localScale = new Vector3(0.01f, 0.01f, 0.01f);
        _finishButton.gameObject.SetActive(false);
    }
    void FinishButtonAppearAnim()
    {
        Sequence starAnimSquence = DOTween.Sequence();
        starAnimSquence.Append(_finishButton.transform.DOScale(1.2f, 1f).SetEase(Ease.OutQuart));
        starAnimSquence.Append(_finishButton.transform.DOScale(1, 1f).OnComplete(OnAnimComplete));
    }
    ///////////////////////
    void Update()
    {
        if (checkAnimEnd)
        {
            if (CatAnimator.GetCurrentAnimatorStateInfo(0).normalizedTime >= 1)
            {
                FinishEnable();
            }
        }
    }
}
