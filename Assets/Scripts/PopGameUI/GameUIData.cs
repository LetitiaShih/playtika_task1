﻿using UnityEngine;

public class GameUIData : BaseGameUIData
{
    protected override void Awake()
    {
        base.Awake();
    }
    protected override void SetUp()
    {
       base.SetUp();
    }
    public override void LoadData()
    {
        base.LoadData();
    }
    public override PopDataItem Getdata()
    {
        base.Getdata();
        return _popDataItem;
    }
    public override void Setdata()
    {
        base.Setdata();
    }
}
