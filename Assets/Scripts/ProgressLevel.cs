﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProgressLevel : MonoBehaviour
{
    float _shiftAmount;
    float _imgWidth; 
    void Start()
    {
        RectTransform rectTransform = GetComponent<RectTransform>();
        _imgWidth = rectTransform.rect.width;
        _shiftAmount = -_imgWidth;
    }
 	void Update () {
		transform.localPosition = new Vector3(_shiftAmount,0f,0f);
	}
    public void UpdateShiftAmount(float progressRatio)
    {
        _shiftAmount = -_imgWidth + (progressRatio) * _imgWidth ;
    }
}
