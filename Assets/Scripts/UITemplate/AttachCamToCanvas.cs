﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[ExecuteInEditMode]
public class AttachCamToCanvas : MonoBehaviour
{
    public string CameraTag;
    private Canvas _canvas;
    private bool _startedWithCamera;

    void Awake()
    {
        _canvas = GetComponent<Canvas>();
        AttachCamera();
    }

 
        private void AttachCamera()
        {
            GameObject obj = GameObject.FindWithTag(CameraTag);
            if (obj != null)
            {
                _canvas.worldCamera = obj.GetComponent<Camera>();
            }
        }


}
