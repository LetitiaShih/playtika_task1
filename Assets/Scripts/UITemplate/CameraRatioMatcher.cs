﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// CameraAspectRatioMatcher controls the camera orthographic ratio to match whats used by the canvas. This allows sprites to render at the same scale as the output PSD does
/// </summary>
[ExecuteInEditMode]
public class CameraRatioMatcher : MonoBehaviour
{
    private const float REF_SIZE = 2.52f;
    private const float WIDTH_MAX = 1.89f;

    private Camera _camera;

    void Update()
    {
        if (_camera == null)
        {
            _camera = GetComponent<Camera>();
        }

        float ratio = (float)Screen.height / (float)Screen.width;
        float designRatio = 768f / 1024f;
        float nr = designRatio / ratio;

        float r = REF_SIZE / nr;
        if (nr > 1)
        {
            _camera.orthographicSize = Mathf.Max(r, WIDTH_MAX);
        }
        else if (nr < 1)
        {
            _camera.orthographicSize = r;
        }
        else
        {
            _camera.orthographicSize = REF_SIZE;
        }

    }
}
