﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class SpriteResizer : MonoBehaviour
{
    void Start()
    {
        float width = Screen.width;
        transform.localScale = Vector3.one * width;
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
