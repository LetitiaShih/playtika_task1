﻿using UnityEngine;
using System;

/// <summary>
/// Now we get data from scritableObject for testing, can also be setted to get from office dataSaving location/adapter
/// </summary>
public class BaseGameUIData : MonoBehaviour
{
    public PopGameData CurrentData;

    protected PopDataItem _popDataItem = new PopDataItem();


    protected virtual void Awake()
    {
        SetUp();
    }
    protected virtual void SetUp()
    {
        LoadData();
    }
    public virtual void LoadData()
    {
        _popDataItem.ProgressBarFullVal = CurrentData.ProgressBarFullVal;
        _popDataItem.ProgressBarVal = CurrentData.ProgressBarVal;
        _popDataItem.BombOn = CurrentData.BombOn;
    }
    public virtual PopDataItem Getdata()
    {
        return _popDataItem;
    }
    public virtual void Setdata()
    {

    }
}
[Serializable]
public class PopDataItem
{
    public int ProgressBarVal;
    public int ProgressBarFullVal;
    public bool BombOn;
}
