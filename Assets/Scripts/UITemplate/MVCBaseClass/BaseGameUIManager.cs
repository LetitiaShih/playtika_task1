﻿
using UnityEngine;
using UserInput;
/// <summary>
/// Define the general funcs a UI manager will need
/// </summary>
public class BaseGameUIManager : MonoBehaviour
{
    GameUIData _gameUIData;
    PopDataItem _popDataItem;
    protected int _progressBarVal;
    protected int _progressBarFullVal;
    protected bool _bombOn;
    public InputManager _inputManager;

    protected virtual void Start()
    {
        _gameUIData = GameObject.FindWithTag("Model").GetComponent<GameUIData>();
        GetData();
    }
    protected virtual void GetData()
    {
        _popDataItem = _gameUIData.Getdata();
        _progressBarFullVal = _popDataItem.ProgressBarFullVal;
        _progressBarVal = _popDataItem.ProgressBarVal;
        _bombOn = _popDataItem.BombOn;

    }
    protected virtual void OnGameFinish()
    {
        //call popUp sene
    }

}
