﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;

/// <summary>
/// Get all Buttons/Texts/Stars, find certain Button/Text
/// Can do: if there are different game can do consistent visual effect at beginning/end 
/// Can do: handle things like setting sorting order dynamically based on whats already open (with the ability to override) 
/// </summary>
public class BaseUIOverlay : MonoBehaviour
{
    protected Button[] _buttons;
    protected Button _findButton;
    protected Text[] _texts;
    protected Text _findText;
    protected Star[] _stars;
    protected Star[] _lightOnStars;
    protected Camera _cam;
    public UnityEvent AnimCompleteEvent;
    protected virtual void Start()
    {
        _cam = GameObject.FindWithTag("UICamera").GetComponent<Camera>();
        GetAllButtons();
        GetAllTexts();
        GetAllStars();
    }
    protected virtual void GetAllButtons()
    {
        _buttons = GetComponentsInChildren<Button>();
    }
    protected virtual void GetCertainButton(string buttonName)
    {
        if (_buttons == null)
        {
            Debug.Log("No Button in Overlay");
        }
        foreach (Button _button in _buttons)
        {
            if (_button.name == buttonName)
            {
                _findButton = _button;
            }
        }
    }
    protected virtual void GetAllTexts()
    {
        _texts = GetComponentsInChildren<Text>();
    }
    protected virtual void GetCertainText(string textName)
    {
        if (_texts == null)
        {
            Debug.Log("No Text in Overlay");
        }
        foreach (Text _text in _texts)
        {
            if (_text.name == textName)
            {

                _findText = _text;
            }
        }
    }
    protected virtual void GetAllStars()
    {
        _stars = GetComponentsInChildren<Star>();
        _lightOnStars = new Star[_stars.Length];

    }
    protected virtual bool CheckButtonDown(Button _button, Vector2 inputDownPos)
    {
        return RectTransformUtility.RectangleContainsScreenPoint(_button.GetComponent<RectTransform>(), inputDownPos, _cam);
    }
    protected virtual void OnAnimComplete()
    {
        AnimCompleteEvent.Invoke();
    }
}
