﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[ExecuteInEditMode]
public class CanvasResizer : MonoBehaviour
{
    private const float LANDSCAPE_X = 675;
    private const float LANDSCAPE_Y = 380;

    private const float LANDSCAPE_X_TK2D = 486;
    private const float LANDSCAPE_Y_TK2D = 366;

    public bool OverrideReferenceResolution = false;
    public bool UsesTK2D = false;

    private CanvasScaler _canvasScaler;

    private void GetComponents()
    {
        _canvasScaler = GetComponent<CanvasScaler>();
    }

    public void Awake()
    {
        Update();
    }

    void Update()
    {
        if (_canvasScaler != null)
        {
            bool widthGreaterThanHeight = Screen.width > Screen.height;

            if (widthGreaterThanHeight)
            {
                float ratio = (float)Screen.width / (float)Screen.height;
                float designRatio = 1024f / 768f;
                float nr = designRatio / ratio;
                if (!OverrideReferenceResolution)
                {
                    if (UsesTK2D)
                    {
                        _canvasScaler.referenceResolution = new Vector2(LANDSCAPE_X_TK2D, LANDSCAPE_Y_TK2D);
                    }
                    else
                    {
                        _canvasScaler.referenceResolution = new Vector2(LANDSCAPE_X, LANDSCAPE_Y);


                    }
                    if (nr < 0.8f)
                    {
                        _canvasScaler.matchWidthOrHeight = 1f;
                    }
                    else
                    {
                        _canvasScaler.matchWidthOrHeight = 0;
                    }
                }
            }
            else
            {
                float ratio = (float)Screen.height / (float)Screen.width;
                float designRatio = 1024f / 768f;
                float nr = designRatio / ratio;
                if (!OverrideReferenceResolution)
                {
                    if (UsesTK2D)
                    {
                        _canvasScaler.referenceResolution = new Vector2(LANDSCAPE_X_TK2D, LANDSCAPE_Y_TK2D);
                    }
                    else
                    {
                        _canvasScaler.referenceResolution = new Vector2(LANDSCAPE_X, LANDSCAPE_Y);


                    }

                    if (nr <= 1f)
                    {
                        _canvasScaler.matchWidthOrHeight = 0;
                    }
                    else
                    {
                        _canvasScaler.matchWidthOrHeight = 1f;
                    }
                }
            }

            if (OverrideReferenceResolution)
            {
                _canvasScaler.matchWidthOrHeight = widthGreaterThanHeight ? 0 : 1;
            }
        }
        else
        {
            GetComponents();
        }
    }
}
