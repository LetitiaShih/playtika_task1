﻿using UnityEngine;
namespace UserInput
{
    public class InputManager : MonoBehaviour
    {
        public bool TapDown = false;
        public bool TapUp = false;
        public Vector2 TapPos = new Vector2(0, 0);


        void Update()
        {
            if (Input.touchCount > 0)
            {
                ProcessTouchEvent();
            }
            else if (Input.mousePresent)
            {
                ProcessMouseEvent();
            }
            else
            {
                NoAction();
            }

        }
        void ProcessTouchEvent()
        {
            if (Input.touchCount >= 1)
            {
                if (Input.touches[0].phase == TouchPhase.Began || Input.touches[0].phase == TouchPhase.Stationary)
                {
                    TapDown = true;
                    TapUp = false;
                    TapPos = Input.touches[0].position;
                    //Debug.Log("TapPos = Input.mousePosition " + TapPos);
                }
                else if (Input.touches[0].phase == TouchPhase.Ended)
                {
                    TapDown = false;
                    TapUp = true;
                }
            }
            else
            {
                NoAction();
            }

        }
        void ProcessMouseEvent()
        {
            if (Input.GetMouseButtonDown(0))
            {
                TapDown = true;
                TapUp = false;
                TapPos = Input.mousePosition;
                //Debug.Log("TapPos = Input.mousePosition " + TapPos);

            }
            else if (Input.GetMouseButtonUp(0))
            {
                TapDown = false;
                TapUp = true;

            }
            else
            {
                NoAction();
            }
        }
        void NoAction()
        {
            TapDown = false;
            TapUp = false;
        }
    }
}